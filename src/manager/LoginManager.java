/**
 * @author Saikat Ghosh
 * 
 * This class is a manager class which manages the login and registration functions
 */
package manager;
import EntityClasses.*;
public class LoginManager
{
	//authenticate is used for login purpose. It returns True for successful Login
	public boolean authenticate(String id,String password) 
		 {
			 user user_ob = new user();
			 boolean b = user_ob.getUserDetails(id);
			 if(b)
			 {String pwd = user_ob.getPassword();
			 //System.out.println(pwd);
			 if(pwd.equals(password))
			 {
				 return true;
			 }
			 else
			 {
				 return false;
			 }}
			 else
				 return false;
		 }	
		 //register is used for registration purpose. Returns true for Successful Registration.
		 public boolean register(String loginID,String first_name,String last_name,String password,String phno,String sex,String address,String dob,String mail)
		 {
			
			 user user_ob = new user();
			 user_ob.getUserDetails(loginID);
			 if(user_ob.getStatus())
			 {
				 return false;
			 }
			 else
			 {
				 user_ob.addUserDetails(loginID,first_name,last_name,password,phno,sex,address,dob,mail);
				 return true;
			 }
		 }
}