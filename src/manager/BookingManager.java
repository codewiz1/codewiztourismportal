/**
 * @author Sagnik Ghosh, Rajarsi Chattopadhyay
 * 
 * This is a manager class which manages the functionalities of view booking,cancel booking
 * booking default tours,making customized tours.
 */
package manager;


import EntityClasses.*;
import java.util.*;

 public class BookingManager {
   
   
	public BookingManager() {
		super();
	}  
	public Booking getBookingDetails(String bookingID,String loginID) //get details about a particular booking
	{
		Booking b = new Booking();
		String packageID = b.getPackageID(bookingID);
		int nooftravelers = b.gettravelers(bookingID);
		Calendar date = b.getdateofjourney(bookingID);
		b.setrecords(packageID, loginID, bookingID, nooftravelers, date);
		return b;
		
		
	}
	public ArrayList<Booking> viewBooking()  //return all bookings made by all users
	{
		Booking b = new Booking();
		ArrayList<Booking> bookings = b.getrecords();
		return bookings;
		
	}
	public  Tour getDetails(String packageID) //get details of a tour given packageID
	{
		
		Tour t=new Tour();
		t.getTour(packageID);
		return t;
		
	}
	public ArrayList<Booking> viewBooking(String loginid)  //return the list of tours booked by a user
	{
		Booking b = new Booking();
		ArrayList<Booking> bookings = b.getrecords(loginid);
		return bookings;
		
	}
	public  void cancelBooking(String bookingID) //cancel a booking given by the bookingID
	{
		Booking b= new Booking();
	    String packageID = b.getPackageID(bookingID);
	    int rooms = b.gettravelers(bookingID)/2;
	    if(rooms==0)
	    	rooms=1;
	    System.out.println(rooms);
	    Tour t = new Tour();
	    t.getTour(packageID);
	    int duration= t.getduration();
		ArrayList<Tour_Details> details = t.getdetails();
		int count = 0;
	    System.out.println(packageID);
	    Calendar cal = b.getdateofjourney(bookingID);
	    while(count<duration)
	    {
	    	Tour_Details detail = details.get(count);
	    	String hotelID = detail.gethotelID();
	    	System.out.println(count + hotelID);
	    	Availability avail = new Availability();
	    	String bookingDate = cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR);
	    	avail.cancelRooms(hotelID, bookingDate, rooms);
	    	cal.add(Calendar.DATE, 1);
	    	count++;
	    }
		
		Traveler tr =new Traveler();
		tr.deleteRecords(bookingID);
		b.deleterecords(bookingID);
	}
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public  boolean checkhotel(Tour tour,Calendar cal,int nooftravelers) //check hotel availability for a particular tour
	{
		boolean possible=false;
		int duration= tour.getduration();
		ArrayList<Tour_Details> details = tour.getdetails();
		int count = 0;
		int reqroom=(nooftravelers)/2;
		while(count<duration)
		{
			Tour_Details detail=details.get(count);
			String hotelID = detail.gethotelID();
			String bookingDate = cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR);
			System.out.println(bookingDate);
			Availability avail = new Availability();
			int rooms = avail.getRooms(hotelID, bookingDate);
			if (rooms>reqroom)
				possible=true; 
			else
				possible=false;
			count++;
			cal.add(Calendar.DATE, 1);
		}
		return possible;
	}
	public Booking book(String packageID,String loginID,int nooftravelers,Calendar cal,String type)
	{
		String bookingID = null;
		Booking book = new Booking();
		int rand = new Random().nextInt(10000);
		if (type.equals("default"))
			bookingID="DEF"+rand;
		else if (type.equals("custom"))
			bookingID="CUS"+rand;
		book.setrecords(packageID, loginID, bookingID, nooftravelers, cal);
		return book;
	}
	public int bookhotel(Booking book,Tour tour)
	{
		ArrayList<Tour_Details> details = tour.getdetails();
		int noofrooms=0;
		if(book.gettravellers()==1)
		{
			noofrooms=1;
		}
		else
			noofrooms = book.gettravellers()/2;
		int duration = tour.getduration();
		Calendar cal = book.getdateofjourney();
		Availability avail = new Availability();
		int count=0;
		while (count<duration)
		{
			if (details.get(count).gethotelID().equals("Train"))
			{	
				count++;
				continue;
			}
			String date = cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR);
			avail.setRooms(details.get(count).gethotelID(), date, noofrooms);
			cal.add(Calendar.DATE, 1);
			count++;
		}
		return 1;
	} 
	public Tour getCustomized(ArrayList<String> packages,Calendar cal,int nooftravelers,int duration, String startinglocation)
	{	
		int saveduration = 0;
		int total = packages.size();
		int count = 0;
		int totaldays = 0;
		int day = 1;
		int needed = 0;
		String name = "Custom";
		Tour arr[] = new Tour[total];
		int i=0;
		int price = 0;
		while(count<total)
		{
			arr[count]=getDetails(packages.get(count));    //this while loop populates default tours and total days 
			totaldays=totaldays+arr[count].getduration(); //needed to complete them
			count++;
		}
		count = 0;
		while(count<total)
		{
		//	System.out.println(arr[count].getstartinglocation());	//is necessary resulting in a day off					
		//	System.out.println(startinglocation);
			if(!(arr[count].getstartinglocation().equals(startinglocation))) //checks whether change of location
			{		
				totaldays++;
				//checks if user has submitted more days than the total days needed
				break;
			}
			count++;
		}
		if (duration>totaldays)
			duration=totaldays; 
		needed = duration;
		count=0;
		while (count<total)
		{					
			//this while loop sorts the tours based on given starting location
			if (!(arr[count].getstartinglocation().equals(startinglocation)))
			{
				int j=count+1;
				while (j < total)                     
				{
					if(arr[j].getstartinglocation().equals(startinglocation))
					{
						Tour temp = arr[j];
						arr[j]=arr[count];
						arr [count] = temp;
					}
					j++;
				}
			}
			count++;
		}
		count=0;
		saveduration = duration;
		while(count<total)    
		{
			name = name +"-"+arr[count].getname();
			System.out.println("Tour days left:"+duration);
			System.out.println("Total needed:"+totaldays);
			float val = (float)(arr[count].getduration()*duration)/(float)totaldays;
			int value=0;
			System.out.println(arr[count].getname()+": duration-"+arr[count].getduration()+",alloted-"+val);
			//if ((val%1)>.5)
				//value = (int)val+1;
			//else 							//this while loop allots days to the packages	
				value = (int)val;
			if (value==0)
				value=1;
			arr[count].setalloted(value);
			System.out.println("Alloted"+arr[count].getalloted());
			price = price + (arr[count].getprice()*arr[count].getalloted())/arr[count].getduration();
			duration=duration-value;
			totaldays=totaldays-arr[count].getduration(); 
			count++;
		}
		count=0;
		while (count<total)
		{
			System.out.println(arr[count].getstartinglocation());
			count++;
		}
		System.out.println("Days left:"+duration);
		duration = saveduration;
		count=0;
		Tour t = new Tour();
		ArrayList<Tour_Details> details = new ArrayList<Tour_Details>();
		int rand = new Random().nextInt(10000);
		String packageID = "CUS"+rand;
		String previous = startinglocation;
		System.out.println(packageID);
		while ((count<total) & (day<=duration))
		{
			Tour temp = arr[count];
			if (!(previous.equals(temp.getstartinglocation())))
			{
				Tour_Details detail = new Tour_Details(packageID,day,"Train","Overnight Journey By Train");
				details.add(detail);
				System.out.println(detail.getday()+"  " + detail.getdetail());
				cal.add(Calendar.DATE, 1);
				System.out.println("Date:"+cal.get(Calendar.DATE)+" "+day);
				day++;
			}
			previous = temp.getstartinglocation();
			System.out.println(temp.getname());
			i = 0;
			while(i<temp.getalloted())
			{
				String bookingDate = cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR);
				System.out.println("Trying to book the following date:"+bookingDate);
				int j = 0;
				int status = 0;
				while(j<temp.getduration() & status==0 )
				{
					if(temp.getdetails().get(j).getstatus()==1)
					{	
						String hotelID = temp.getdetails().get(j).gethotelID();
						System.out.println(hotelID);
						Availability avail = new Availability();
						int reqroom = (nooftravelers)/2;
						if (reqroom==0)
							reqroom=1;
						int rooms = avail.getRooms(hotelID, bookingDate);
						if (reqroom<=rooms)
						{
							System.out.println("Booking available on the following date:"+bookingDate);
							temp.getdetails().get(j).changestatus();
							Tour_Details detail = new Tour_Details(packageID,day,hotelID,temp.getdetails().get(j).getdetail());
							details.add(detail);
							System.out.println(detail.getday()+"  " + detail.gethotelID());
							cal.add(Calendar.DATE, 1);
							System.out.println(cal.get(Calendar.DATE)+" "+day);
							day++;
							status = 1;
						}
					}
					j++;
				}
				i++;
				System.out.println(i+"   "+temp.getalloted());
			}
			count++;
		}
		System.out.println(needed);
		t.setTour(packageID, startinglocation, name, price, needed, details);
		return t;
	}
	public void settravelerdetails(String name,int age,String sex,String bookingId) //Set details of Travelers for a booking
	{
		Traveler t=new Traveler();
		t.setRecords(bookingId, name, age, sex);
	}
}