/**
 * @author Sagnik Ghosh,Rajarsi Chattopadhyay
 * 
 * This class is used for unit testing the BookingManager class
 */
package UnitTestClasses;
import EntityClasses.Booking;
import EntityClasses.Tour;
import EntityClasses.Tour_Details;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import manager.BookingManager;
import junit.framework.TestCase;


public class BookingManagerTest extends TestCase {

		public BookingManagerTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*public void testBookingManager() {
		fail("Not yet implemented");
	}*/

	public void testGetDetails() {
		BookingManager b=new BookingManager();
		Tour t2=b.getDetails("NB101");
		
		ArrayList<Tour_Details> td= new ArrayList<Tour_Details>();
		td.add(new Tour_Details("NB101",1,"H101","'Lava is a small hill station in Eastern Himalaya. After checking in and having done lunch, go for Lava local sightseeing covering Ratnarishi Bihar Buddhist Gumpha, Neora Interpretation Centre, Chhengey waterfalls. Overnight at Lava."));
		td.add(new Tour_Details("NB101",2,"H102","Lolegaon is a beautiful village of Eastern Himalaya and famous for his natural beauty. After checking in and completing lunch, Lolegaon sightseeing covering Heritage Forest and nearby areas. Trasnfer to Rishyop at evening. Overnight at Rishyop.Watch the beautiful Kanchanzungha sunrise from your hotel room next morning."));
		Tour t = new Tour("NB101","West Dooars","NJP",10000,2,td);
		assertEquals(t.getname(),t2.getname());
		assertEquals(t.getprice(),t2.getprice());
		assertEquals(t.getduration(),t2.getduration());
		assertEquals(t.getid(),t2.getid());
		//assertEquals(t.getdetails(),t2.getdetails());
		
		}

   public void testViewBooking() {
	   BookingManager b=new BookingManager();
	   ArrayList<Booking> book = b.viewBooking("test");
	   ArrayList<Booking> test = new ArrayList<Booking>();
	   SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
		String dateInString = "23-4-2016";
		Date date=null;
		try {
			date = sdf.parse(dateInString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
	   Booking b1 = new Booking("SB101","test","DEF93",3,cal);
	   test.add(b1);
	   dateInString = "22-4-2016";
	   date=null;
		try {
			date = sdf.parse(dateInString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    cal = Calendar.getInstance();
		cal.setTime(date);
		Booking b2 = new Booking("NB103","test","DEF4310",1,cal);
		test.add(b2);
		int count=0;
		for(Booking i : book) {
			
			assertEquals(i.gettravellers(),test.get(count).gettravellers());
			assertEquals(i.getbookingID(),test.get(count).getbookingID());
			assertEquals(i.getpackageID(),test.get(count).getpackageID());
			assertEquals(i.getdateofjourney(),test.get(count).getdateofjourney());
			count++;
			//assertEquals(i.
		}
}
   public void testCheckHotel()
   {
	   BookingManager b= new BookingManager();
	   Tour t = new Tour();
	   t.getTour("NB100");
	   SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
	   String dateInString = "30-4-2016";
		Date date=null;
		try {
			date = sdf.parse(dateInString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    Calendar cal = Calendar.getInstance();
		cal.setTime(date);
	   assertTrue(b.checkhotel(t, cal, 1));
	  // assertTrue(b.checkhotel(t, cal, 8));
   }

/*	public void testCancelBooking() {
		fail("Not yet implemented");
	}*/

}
