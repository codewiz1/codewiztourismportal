/**
 * @author Saikat Ghosh
 * 
 * This class is used for unit testing the LoginManager class
 */
package UnitTestClasses;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import manager.*;

/**
 * @author Saikat Ghosh
 *
 */
public class LoginManagerTest {
     LoginManager l;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link manager.LoginManager#authenticate(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAuthenticate() {
	   l= new LoginManager();
	   boolean b = l.authenticate("test", "test");
	   assertTrue(b);
	   b = l.authenticate("test", "te");
	   assertFalse(b);
	   b = l.authenticate("test1", "te");
	   assertFalse(b);
	}

	/**
	 * Test method for {@link manager.LoginManager#register(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRegister() {
		l = new LoginManager();
		boolean b= l.register("mama","Rajarsi" ,"Chattopadhyay","abcd", "1234567890", "male", "sas", "aa", "");
		assertFalse(b);
		
	}

}
