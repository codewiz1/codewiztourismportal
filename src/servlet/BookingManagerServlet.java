/**
 * @author Sagnik Ghosh,Rajarsi Chattopadhyay 
 */
package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import EntityClasses.*;

import java.text.SimpleDateFormat;
import java.util.*;
import manager.*;

 public class BookingManagerServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;
   BookingManager b;
    
	public BookingManagerServlet() {
		super();
	}   	
	public void init()
	{
		 b=new BookingManager();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}  	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String sub = request.getParameter("func");
		if (sub.equals("viewBooking")) //for viewing booked tours
		{
			HttpSession session = request.getSession();
			ArrayList<Booking> bookings = b.viewBooking((String)session.getAttribute("unm"));
			session.setAttribute("list", bookings);
			response.sendRedirect("ViewCancelBooking.jsp");
			
		}
		else if(sub.equals("viewcancel")) 
		{
			if (request.getParameter("cancel")!=null) //canceling a booked tour
			{
			response.setContentType("text/html");

		      // Actual logic goes here.
		      //PrintWriter out = response.getWriter();
			//out.println("CANCEL BookingID: "+request.getParameter("cancel") );
			b.cancelBooking(request.getParameter("cancel"));
			response.sendRedirect("cancel.jsp");
			}
			else if (request.getParameter("view")!=null) //view details of a particular tour
			{
				HttpSession session = request.getSession();
				//PrintWriter out = response.getWriter();
				String bookingID= request.getParameter("view");
				//out.println("Booking ID:"+bookingID);
				Booking book = new Booking();
				String packageID = book.getPackageID(bookingID);
				Booking bk = b.getBookingDetails(bookingID,(String)session.getAttribute("unm"));
				Tour t=b.getDetails(packageID);
				//out.println("View BookingID: "+request.getParameter("view") );
				//out.println("Package name "+t.getname() );
				//quest.setAttribute("tour", t);
				//RequestDispatcher rd=request.getRequestDispatcher("/ViewDetails.jsp");    
				//rd.forward(request, response);//method may be include or forward  
				
				session.setAttribute("tour", t);
				session.setAttribute("bookingdet",bk);
				response.sendRedirect("ViewDetails.jsp");
				
			}
		}
		else if (sub.equals("defaultSearch")) //for default tour booking
		{
			Tour tour = b.getDetails(request.getParameter("packageID"));
			//RequestDispatcher rd = request.getRequestDispatcher("/TourInfo.jsp");
			//request.setAttribute("tour", tour);
			//rd.forward(request, response);
			HttpSession session = request.getSession();
			session.setAttribute("tour", tour);
			response.sendRedirect("TourInfo.jsp");
		}
		else if  (sub.equals("customSearch"))
		{
			ArrayList<String> packages = new ArrayList<String>();
			/*int count=Integer.parseInt(request.getParameter("count"));
			
			int i = 0;
			while(i<count)
			{	
				packages.add((String)request.getParameter("packageID"+(i+1)));
				//System.out.println(packages.get(i));
				i++;
			}*/
			int count=0;
			if(request.getParameter("c1") != null)
			{
				packages.add((String)request.getParameter("c1"));
				count++;
			}
			if(request.getParameter("c2") != null)
			{
				packages.add((String)request.getParameter("c2"));
				count++;
			}
			if(request.getParameter("c3") != null)
			{
				packages.add((String)request.getParameter("c3"));
				count++;
			}
			if(request.getParameter("c4") != null)
			{
				packages.add((String)request.getParameter("c4"));
				count++;
			}
			if(request.getParameter("c5") != null)
			{
				packages.add((String)request.getParameter("c5"));
				count++;
			}
			if(request.getParameter("c6") != null)
			{
				packages.add((String)request.getParameter("c6"));
				count++;
			}
			count++;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
			String dateInString = request.getParameter("dateofjourney");
			Date date=null;
			try {
				date = sdf.parse(dateInString);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			//System.out.print(date);
			 Calendar cal = Calendar.getInstance();
			 cal.setTime(date);
			 Calendar copy = Calendar.getInstance();
			 copy.setTime(date);
	    	 System.out.println("After setting Time:  " + cal.get(Calendar.DATE));
			 int nooftravelers = Integer.parseInt(request.getParameter("nooftravelers"));
			 //System.out.println(nooftravelers);
			 //String loginID = request.getParameter("loginID");
			 int duration =Integer.parseInt(request.getParameter("duration"));
			 //System.out.println(duration);
			 String startinglocation = request.getParameter("startinglocation");
			 Tour tour = b.getCustomized(packages, cal, nooftravelers, duration, startinglocation);
			 HttpSession session = request.getSession();
			 session.setAttribute("tour", tour);
			 session.setAttribute("cal", copy);
			 session.setAttribute("nooftravelers", nooftravelers);
			 System.out.println(tour.getdetails().size());
			 for (int l=0;l<tour.getdetails().size();l++)
			 System.out.println(tour.getdetails().get(l).gethotelname());
			 if (tour.getduration()==tour.getdetails().size())
				 response.sendRedirect("Info.jsp");
			 else
				 response.sendRedirect("noTour.jsp");
		}
		else if (sub.equals("book")) //booking default tour
		{
			HttpSession session = request.getSession();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
			String dateInString = request.getParameter("dateofjourney");
			//session.setAttribute("bdate",request.getParameter("dateofjourney"));
			Date date=null;
			try {
				date = sdf.parse(dateInString);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			//System.out.print(date);
			 Calendar cal = Calendar.getInstance();
			 cal.setTime(date);
			 String dat = cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR);
				System.out.println(date);
				session.setAttribute("bdate",dat);
	    	 System.out.println("After setting Time:  " + cal.get(Calendar.DATE));
			 int nooftravelers = Integer.parseInt(request.getParameter("nooftravelers"));
			 String loginID = request.getParameter("loginID");
			 
			 
			 if(session.getAttribute("tour") == null)
			 {
				 response.sendRedirect("login.html");
			 }
			 else{
			 Tour tour = (Tour)session.getAttribute("tour");
			 boolean possible = b.checkhotel(tour, cal, nooftravelers);
			 System.out.println(possible);
			 cal.setTime(date);
			 if (possible)
			 {
				 Booking book=b.book(tour.getid(), loginID, nooftravelers, cal,"default");
				 //book.dbInsert();
				 session.setAttribute("booking", book);
				 session.setAttribute("nooftravelers", nooftravelers);
				 response.sendRedirect("TravelerInfo.jsp");
			 }
			 else
			 {
				 PrintWriter out = response.getWriter();
				 out.println("Sorry Hotel Not availabile");
				 response.sendRedirect("noHotel.jsp");
			 }
		}
	}
		else if (sub.equals("save")) //saving records of travelers
		{
			HttpSession session = request.getSession();
			 if(session.getAttribute("booking") == null)
			 {
				 response.sendRedirect("login.html");
			 }else
			 {	Booking book = (Booking)session.getAttribute("booking");
			System.out.println(book.getdateofjourney().get(Calendar.DATE));
			book.dbInsert();
			Tour tour = (Tour)session.getAttribute("tour");
			b.bookhotel(book, tour);
			int travelercount=(Integer)session.getAttribute("nooftravelers");
			while (travelercount>0)
			{
				
				String name=(String)request.getParameter("name"+travelercount);
				int age= Integer.parseInt(request.getParameter("age"+travelercount));
				String sex= (String)request.getParameter("sex"+travelercount);
				//System.out.print(name+sex+age+travelercount);
				b.settravelerdetails(name, age, sex, book.getbookingID());
				travelercount--;
			}
			PrintWriter out = response.getWriter();
			out.println("Booking Confirmed");
			response.sendRedirect("bookingConfirmed.jsp");
		}
		}
		else if (sub.equals("bookCustom"))
		{
			HttpSession session = request.getSession();
			Tour tour = (Tour)session.getAttribute("tour");
			String loginID = request.getParameter("loginID");
			int travelercount=(Integer)session.getAttribute("nooftravelers");
			Calendar cal = (Calendar)session.getAttribute("cal");
			String date = cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR);
			System.out.println(date);
			session.setAttribute("bdate",date);
			tour.dbInsert();
			System.out.print(tour.getid());
			Booking book=b.book(tour.getid(), loginID, travelercount, cal,"custom");
			session.setAttribute("booking", book);
			book.dbInsert();
			b.bookhotel(book, tour);
			while (travelercount>0)
			{
				String name=(String)request.getParameter("name"+travelercount);
				int age= Integer.parseInt(request.getParameter("age"+travelercount));
				String sex= (String)request.getParameter("sex"+travelercount);
				//System.out.print(name+sex+age+travelercount);
				b.settravelerdetails(name, age, sex, book.getbookingID());
				travelercount--;
			}
			//out.println("Booking Confirmed");
			response.sendRedirect("bookingConfirmed.jsp");
		}
		else if(sub.equals("adminView"))
		{
			if(request.getParameter("viewAll")!=null)
			{
				HttpSession session = request.getSession();
				ArrayList<Booking> bookings = b.viewBooking();
				session.setAttribute("list", bookings);
				response.sendRedirect("viewAdmin.jsp");
			}
		}
	}	    
}