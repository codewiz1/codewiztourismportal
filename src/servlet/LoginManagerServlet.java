/**
 * @author Saikat Ghosh
 * 
 * This is a controller class which is used for Login and registration purposes.
 */

package servlet;
import java.io.IOException;  
import javax.servlet.RequestDispatcher;  
import javax.servlet.ServletException;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;  
import javax.servlet.http.HttpSession;

import manager.*;
public class LoginManagerServlet extends HttpServlet 
{  
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
        response.setContentType("text/html");  
         

		if (request.getParameter("login") != null) 
			{
            //perform login
			 String loginID =request.getParameter("userid");  
             String password=request.getParameter("pwd");  
			 LoginManager bean = new LoginManager();
			 boolean status= bean.authenticate(loginID,password);
			  if(status)
			  {
				  HttpSession session = request.getSession();
				  session.setAttribute("unm", loginID);
				  if(loginID.equals("admin"))
				  {
					  response.sendRedirect("adminHome.jsp");
					  session.setAttribute("id", session.getId());
					  System.out.print(session.getId());
				  }
				  else{
				  response.sendRedirect("home.jsp");
				  session.setAttribute("id", session.getId());
				  System.out.print(session.getId());
               }  
			  }
        else{  
            RequestDispatcher rd=request.getRequestDispatcher("login-error.jsp");  
            rd.forward(request, response);  
             }
            }
        else if (request.getParameter("register") != null) 
		{
           // Perform register
		     String loginID = request.getParameter("userid");
             String first_name= request.getParameter("fname");
             String last_name = request.getParameter("lname"); 
             String password = request.getParameter("pwd");
             String phno = request.getParameter("phno");
             String sex = request.getParameter("sex");
             String address = request.getParameter("address"); 	
             String dob = request.getParameter("dob");
             String mail = request.getParameter("email");
             
			 LoginManager bean= new LoginManager();
			 boolean status=bean.register(loginID,first_name,last_name,password,phno,sex,address,dob,mail);
			  if(status)
			  {  
           // RequestDispatcher rd=request.getRequestDispatcher("register_success.jsp");  
            //rd.forward(request, response);  
				  HttpSession session = request.getSession();
				  session.setAttribute("unm", loginID);
				  response.sendRedirect("home.jsp");
			  }  
			  else{  
            RequestDispatcher rd=request.getRequestDispatcher("register-error.jsp");  
            rd.forward(request, response);  
             }

		  }
		}
		 

}		 