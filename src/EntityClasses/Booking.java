/**
 * @author Sagnik Ghosh
 * 
 * Booking,java is an entity class which manages the entity Booking
 */

package EntityClasses;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Booking {
	
private String packageID;
private String loginID;
public String bookingID;
private int nooftravelers;
private Calendar dateofjourney;

public Booking()
{
	
}
public Booking(String packageID,String loginID,String bookingID,int nooftravelers,Calendar dateofjourney)
{
    
	this.packageID  = packageID;
    this.loginID = loginID;
    this.bookingID = bookingID;
    this.nooftravelers = nooftravelers;
    this.dateofjourney = dateofjourney;
    
    
}
public int gettravellers()
{
	return nooftravelers;
}
public String getLoginId()
{
	return loginID;
}

public String getpackageID()
{
	return packageID;
}

public Calendar getdateofjourney()
{
	return dateofjourney;
}

public String getpackageName()
{
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	String temp=null;
	Statement st=null;
	ResultSet rs=null;
	try {
		st = con.createStatement();
		rs= st.executeQuery("select package_name from tour where package_id='"+this.packageID+"'");
		if(rs.next())
		{
			temp = rs.getString(1);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
		if (rs != null) {
	        try {
	            rs.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (st != null) {
	        try {
	            st.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
	return temp;
}

public String getbookingID()
{
	return bookingID;
}

public int setrecords(String packageID,String loginID,String bookingID,int nooftravelers,Calendar dateofjourney)
{
	this.bookingID = bookingID;
	this.loginID = loginID;
	this.packageID = packageID;
	this.nooftravelers = nooftravelers;
	this.dateofjourney = dateofjourney;
	return 1;
}
public int deleterecords(String bookingID)
{
	this.bookingID = bookingID;
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	PreparedStatement ps = null;
	try {
		ps = con.prepareStatement("delete from booking where booking_id = ? ");
		ps.setString(1,this.bookingID);
		ps.executeUpdate();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
	    if (ps != null) {
	        try {
	            ps.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
	return 1;
}
public ArrayList<Booking> getrecords(String loginID)
{
	ArrayList<Booking> bookings = new ArrayList<Booking>();
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	Statement st = null;;
	ResultSet rs = null;
	try {
		st = con.createStatement();
		rs= st.executeQuery("select * from booking where login_id='"+loginID+"'");
		while(rs.next())
		{
			Booking book = new Booking();
			book.bookingID = rs.getString(1);
			book.packageID = rs.getString(2);
			book.loginID = rs.getString(3);
			String temp=rs.getString(4);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
			String dateInString = temp;
			Date date=null;
			try {
				date = sdf.parse(dateInString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			book.dateofjourney = cal;
			book.nooftravelers = rs.getInt(5);
			bookings.add(book);
			System.out.println(book.bookingID);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
		if (rs != null) {
	        try {
	            rs.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (st != null) {
	        try {
	            st.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
    return bookings;
}
public ArrayList<Booking> getrecords()
{
	ArrayList<Booking> bookings = new ArrayList<Booking>();
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	Statement st = null;;
	ResultSet rs = null;
	try {
		st = con.createStatement();
		rs= st.executeQuery("select * from booking");
		while(rs.next())
		{
			Booking book = new Booking();
			book.bookingID = rs.getString(1);
			book.packageID = rs.getString(2);
			book.loginID = rs.getString(3);
			String temp=rs.getString(4);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
			String dateInString = temp;
			Date date=null;
			try {
				date = sdf.parse(dateInString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			book.dateofjourney = cal;
			book.nooftravelers = rs.getInt(5);
			bookings.add(book);
			System.out.println(book.bookingID);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
		if (rs != null) {
	        try {
	            rs.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (st != null) {
	        try {
	            st.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
    return bookings;
}
public String getPackageID(String bookingID)
{
	//ArrayList<Booking> bookings = new ArrayList<Booking>();
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	Statement st = null;
	ResultSet rs = null;
	String packageID=null;
	try {
		st = con.createStatement();
		rs= st.executeQuery("select package_id from booking where booking_id='"+bookingID+"'");
		if(rs.next())
		{
			packageID= rs.getString(1);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
		if (rs != null) {
	        try {
	            rs.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (st != null) {
	        try {
	            st.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
    return packageID;
}

public Calendar getdateofjourney(String bookingID)
{
	//ArrayList<Booking> bookings = new ArrayList<Booking>();
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	Statement st = null;
	ResultSet rs = null;
	String dat=null;
	try {
		st = con.createStatement();
		rs= st.executeQuery("select date_of_journey from booking where booking_id='"+bookingID+"'");
		if(rs.next())
		{
		dat= rs.getString(1);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
		if (rs != null) {
	        try {
	            rs.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (st != null) {
	        try {
	            st.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
	DateFormat df = new SimpleDateFormat("dd-MM-yyyy"); 
    Date date;
    Calendar cal = null ;
    try {
		date = df.parse(dat);
		cal = Calendar.getInstance();
		cal.setTime(date);
		  
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  return cal;
}

 
public int gettravelers(String bookingID)
{
	//ArrayList<Booking> bookings = new ArrayList<Booking>();
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	Statement st = null;
	ResultSet rs = null;
	int t=0;
	try {
		st = con.createStatement();
		rs= st.executeQuery("select total_travellers from booking where booking_id='"+bookingID+"'");
		if(rs.next())
		{
		t= rs.getInt(1);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
		if (rs != null) {
	        try {
	            rs.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (st != null) {
	        try {
	            st.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
	return t;
}

//dbInsert used to Insert values in Booking Table
public void dbInsert()
{
	ConnectDB c = new ConnectDB();
	Connection con = c.connect();
	PreparedStatement ps = null;
	try {
		ps = con.prepareStatement("insert into booking values (?,?,?,?,?)");
		ps.setString(1, bookingID);
		ps.setString(2, packageID);
		ps.setString(3, loginID);
		String temp = dateofjourney.get(Calendar.DATE) +"-"+ (dateofjourney.get(Calendar.MONTH)+1)+"-"+dateofjourney.get(Calendar.YEAR);
		System.out.println(temp);
		ps.setString(4, temp);
		ps.setInt(5, nooftravelers);
		ps.executeUpdate();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally{
	    if (ps != null) {
	        try {
	            ps.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) { /* ignored */}
	    }
	}
}
}

