/**
 * @author Sagnik Ghosh
 * 
 * Tour.java is an entity class which manages the entity Tour
 */
package EntityClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Tour {
	private String id;
	private String startinglocation;
	private String name;
	private int duration;
	private int price;
	private ArrayList<Tour_Details> details;
	private int alloted;
	public Tour()
	{
		
	}
	public Tour(String id,String name,String startinglocation,int price,int duration,ArrayList<Tour_Details> details)
	{
		this.id=id;
		this.startinglocation=startinglocation;
		this.name=name;
		this.duration=duration;
		this.price=price;
		this.details=details;
	}
	public void setalloted(int value)
	{
		alloted=value;
	}
	public int getalloted()
	{
		return alloted;
	}
	public ArrayList<Tour_Details> getdetails()
	{
		return details;
	}
	
	public int getprice()
	{
		return price;
	}
	
	public int getduration()
	{
		return duration;
	}
	
	public String getname()
	{
		return name;
	}
	
	public String getstartinglocation()
	{
		return startinglocation;
	}
	public String getid()
	{
		return id; 
	}
	public void getTour(String packageID)
	{
		//System.out.println(packageID);
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement stmt=null;
		ResultSet rs=null;
		this.id=packageID;
		try {
			stmt=con.createStatement();
			rs= stmt.executeQuery("select * from tour where package_id='"+packageID+"'");
			if(rs.next())
			{this.name=rs.getString(2);
			this.startinglocation=rs.getString(3);
			this.price=rs.getInt(4);
			this.duration=rs.getInt(5);
			}
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		Tour_Details temp = new Tour_Details();
		this.details=temp.getTourdetails(this.id);
	}
	
	public int setTour(String id,String startinglocation,String name,int price,int duration, ArrayList<Tour_Details> details)
	{
		this.startinglocation = startinglocation;
		this.name = name;
		this.id = id;
		this.price = price;
		this.duration = duration;
		this.details = details;
		return 1;
	}
	public void dbInsert()
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("insert into tour values (?,?,?,?,?)");
			ps.setString(1, id);
			ps.setString(2, name);
			ps.setString(3, startinglocation);
			ps.setInt(4, price);
			ps.setInt(5, duration);
			ps.executeUpdate();
			System.out.println("done tour");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
		    if (ps != null) {
		        try {
		            ps.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		int count = details.size();
		int i = 0;
		while(i<count)
		{
			Tour_Details temp = details.get(i);
			int success = temp.setdetails();
			if (success == 0)
				System.out.print("done");
			i++;
		}
	}
}
