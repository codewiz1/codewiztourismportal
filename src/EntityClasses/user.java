/**
 * @author Saikat Ghosh
 * 
 * user.java is an entity class which manages the entity User
 */
package EntityClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class user {

private String loginID;
private String first_name;
private String last_name;
private String password;
private String phno;
private String sex;
private String address; 	
private String dob;
private String mail;
private boolean status;

     public String getName()
       {
	     return first_name +""+last_name; 
       }	
	
	public String getPassword()
	{
		return password;
	}

	public String getmail()
	{
		return mail;
	}
	public String getdob()
	{
		return dob;
	}
	public String getaddress()
	{
		return address;
	}
	public String getsex()
	{
		return sex;
	}
	public String getphno()
	{
		return phno;
	}
	public boolean getStatus()
	{
		return status;
	}
	
	public boolean getUserDetails(String loginID)
	{
	  ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement stmt=null;
		ResultSet rs=null;
		status=false;
		this.loginID=loginID;
		try {
			stmt=con.createStatement();
			rs= stmt.executeQuery("select * from users where login_id='"+loginID+"'");	
			if(rs.next())
			{ 
		     status= true;
		     this.loginID = rs.getString(1);
			this.first_name=rs.getString(2);
			this.last_name=rs.getString(3);
			this.password = rs.getString(4);
			this.address = rs.getString(5);
			this.sex = rs.getString(6);
			this.dob = rs.getString(7);
			this.phno =rs.getString(8);
			this.mail = rs.getString(9);
			return true;
			}
			else{
				return false;
			}
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return status;
	}
	
	public void addUserDetails(String loginID,String first_name,String last_name,String password,String phno,String sex,String address,String dob, String mail)
	{
		this.loginID = loginID;
		this.first_name = first_name;
		this.last_name = last_name;
		this.password = password;
		this.phno = phno;
		this.sex = sex;
		this.address = address;
		this.dob = dob;
		this.mail = mail;
		
		
		
		dbInsert();
		
		
	}
	
	private void dbInsert()
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("insert into users values (?,?,?,?,?,?,?,?,?)");
			ps.setString(1, loginID);
			ps.setString(2, first_name);
			ps.setString(3, last_name);
			ps.setString(4, password);
			ps.setString(5, address);
			ps.setString(6, sex);
			ps.setString(7,dob);
			ps.setString(8,phno);
			ps.setString(9, mail);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
		    if (ps != null) {
		        try {
		            ps.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
	}
	
	
}