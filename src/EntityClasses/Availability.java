/**
 * @author Rajarsi Chattopadhyay
 * 
 * Availability.java is an entity class which manages the entity Availability
 */
package EntityClasses;
import java.sql.*;

	
public class Availability {
	
	String date;
	String hotelID;
	int roomsRemaining;
	
//getRooms is used to retrieve no of empty rooms of a hotel on a given date	
	public int getRooms(String hotelID,String bookingDate)
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement st = null;
		ResultSet rs=null;
		//int rooms=0;
		try{
			st = con.createStatement();
		 	rs= st.executeQuery("Select * from availability where booking_date='"+bookingDate+"' and hotel_id='"+hotelID+"'");
		 	if(rs.next())
		 	{
		 		roomsRemaining= rs.getInt(3);
		 	}
		 	else
		 		roomsRemaining=5;
			
		}
		catch(Exception e)
		{
		
		}
		finally{
		    if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (st != null) {
		        try {
		            st.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return roomsRemaining;
		
	}
	//cancelRooms is used for updating Availability when a booking is cancelled
	public int cancelRooms(String hotelID,String bookingDate,int rooms)
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement st = null;
		ResultSet rs=null;
		PreparedStatement ps=null;
		try{
			st = con.createStatement();
		 	rs= st.executeQuery("Select * from availability where booking_date='"+bookingDate+"' and hotel_id='"+hotelID+"'");
		 	if(rs.next())
		 	{
		 		int roomsremain = rs.getInt(3);
		 		ps = con.prepareStatement("update availability set rooms_remaining=? where booking_date='"+bookingDate+"' and hotel_id='"+hotelID+"'");
		 		ps.setInt(1, roomsremain+rooms);
		 		int s= ps.executeUpdate();
		 		if(s==1)
		 			return 1;
		 		else
		 			return 0;
		 	}
		 	else
		 	{
		 		return 0;
		 	}		
		}
		catch(Exception e)
		{
		
		}
		finally{
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
			if (st != null) {
		        try {
		            st.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (ps != null) {
		        try {
		            ps.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return 0;
	}
	
	//setRooms is used for updating Availability when a room is booked
	public int setRooms(String hotelID,String bookingDate,int rooms)
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement st = null;
		ResultSet rs=null;
		PreparedStatement ps=null;
		try{
			st = con.createStatement();
		 	rs= st.executeQuery("Select * from availability where booking_date='"+bookingDate+"' and hotel_id='"+hotelID+"'");
		 	if(rs.next())
		 	{
		 		int roomsremain = rs.getInt(3);
		 		ps = con.prepareStatement("update availability set rooms_remaining=? where booking_date='"+bookingDate+"' and hotel_id='"+hotelID+"'");
		 		ps.setInt(1, roomsremain-rooms);
		 		int s= ps.executeUpdate();
		 		if(s==1)
		 			return 1;
		 		else
		 			return 0;
		 	}
		 	else
		 	{
		 		ps= con.prepareStatement("insert into availability values(?,?,?)");
		 		ps.setString(1, bookingDate);
		 		ps.setString(2, hotelID);
		 		ps.setInt(3, 5-rooms);
		 	/*	System.out.print(hotelID);
		 		System.out.print(bookingDate);
		 		System.out.print(5-rooms);
		 		System.out.print("hello");*/
		 		int s = ps.executeUpdate();
		 		if (s==1)
		 			return 1;
		 		else
		 			return 0;
		 	}		
		}
		catch(Exception e)
		{
		
		}
		finally{
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
			if (st != null) {
		        try {
		            st.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (ps != null) {
		        try {
		            ps.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return 0;
	}
}