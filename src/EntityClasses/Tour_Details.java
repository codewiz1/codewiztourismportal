/**
 * @author Sagnik Ghosh
 * 
 * Tour_Details.java is an entity class which manages the entity TourDetails
 */
package EntityClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Tour_Details {
	private String id;
	private int day;
	private String hotelID;
	private String detail;
	private int status = 1;
	
	public Tour_Details()
	{
		
	}
	public Tour_Details(String id,int day,String hotelID,String detail)
	{
	   this.id=id;
	   this.day=day;
	   this.hotelID = hotelID;
	   this.detail=detail;
	}
	public void changestatus()
	{
		status=0;
	}
	public int getstatus()
	{
		return status;	
	}
	public int getday()
	{
		return day;
	}
	public String gethotelID()
	{
		return hotelID;
	}
	public String getID()
	{
		return id;
	}
	
	public String getdetail()
	{
		return detail;
	}
	
	public String gethotelname()
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement stmt=null;
		ResultSet rs = null;
		String temp=null;
		try {
			stmt = con.createStatement();
		    rs= stmt.executeQuery("select name from hotels where hotel_id='"+this.hotelID+"'");
			if(rs.next())
			{
				temp = rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return temp;
	}
	
	public ArrayList<Tour_Details> getTourdetails(String packageID)
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		ArrayList<Tour_Details> details = new ArrayList<Tour_Details>();
		Statement stmt=null;
		ResultSet rs=null;
		try{
			stmt=con.createStatement();
			rs= stmt.executeQuery("select * from tourDetails where package_id='"+packageID+"'");
			while(rs.next())
			{
				Tour_Details temp = new Tour_Details();
				temp.id=packageID;
				temp.day=rs.getInt(2);
				temp.hotelID=rs.getString(3);
				temp.detail=rs.getString(4);
				details.add(temp);
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return details;
	}
	
	public int setdetails()
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("insert into tourDetails values (?,?,?,?)");
			ps.setString(1, this.id);
			ps.setInt(2, this.day);
			ps.setString(3, this.hotelID);
			ps.setString(4, this.detail);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
		    if (ps != null) {
		        try {
		            ps.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return 0;
	}
}
