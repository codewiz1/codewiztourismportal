/**
 * @author Sagnik Ghosh
 * 
 * ConnectDB class is used to make the connection with the DataBase
 */
package EntityClasses;


import java.sql.Connection;
import java.sql.*; 

public class ConnectDB {
	public Connection connect()
	{
		Connection con=null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver"); 
			con=DriverManager.getConnection(  
			"jdbc:oracle:thin:@localhost:1522:xe","hr","hr"); 
		}catch(Exception e)
		{
			System.out.print(e);
		}
		return con;
	}
}
