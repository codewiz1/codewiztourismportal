/**
 * @author Rajarsi Chattopadhyay
 * 
 * Hotel.java is an entity class which manages the entity Hotel
 */
package EntityClasses;
import java.sql.*;
	
public class Hotel {

    String id;
    String name;
	String place;
    int price;
	
    public String getName()
    {
    	return name;
    }
    public String getid()
    {
    	return id;
    }
    public String getplace()
    {
    	return place;
    }
    public int getprice()
    {
    	return price;
    }
    
	public void getHotelDetails(String hid)
	{
		try{
			ConnectDB c = new ConnectDB();
			Connection con = c.connect();
			Statement st = con.createStatement();
			ResultSet rs;
			rs= st.executeQuery("Select * from hotels where hotel_id='"+hid+"'");
			if(rs.next())
			{
	 		this.id = rs.getString(1);
	 		this.name = rs.getString(1);
	 		this.place = rs.getString(3);
	 		this.price = rs.getInt(4);
			}
		}
		catch(Exception e)
		{
		    System.out.println(e);	
		}
	
	}
}
