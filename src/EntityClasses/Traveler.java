/**
 * @author Rajarsi Chattopadhyay
 * 
 * Traveler.java is an entity class which manages the entity Traveler
 */
package EntityClasses;
import java.sql.*;
import java.util.ArrayList;



public class Traveler {
	
	String bookingID;
	int age;
	String name;
	String sex;
	//
	public int getAge()
	{
		return age;
	}
	public String getName()
	{
		return name;
	}
	public String getSex()
	{
		return sex;
	}
	public int deleteRecords(String bookingID)
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement st = null;
		//ResultSet rs = null;
		PreparedStatement ps = null;
		try{
			 	st = con.createStatement();
		 		ps= con.prepareStatement("delete from Traveler where booking_id=?");
		 		ps.setString(1, bookingID);
		 		int s= ps.executeUpdate();
		 		if(s==1)
		 			return 1;
		 		else
		 			return 0;
		 		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			if (st != null) {
		        try {
		           st.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (ps != null) {
		        try {
		            ps.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return 0;
	}
	public int setRecords(String bookingId,String name,int age,String sex)
	{
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		PreparedStatement ps = null;
		try{
		 		ps= con.prepareStatement("insert into Traveler values(?,?,?,?)");
		 		ps.setString(1, bookingId);
		 		ps.setString(2, name);
		 		ps.setInt(3, age);
		 		ps.setString(4,sex);
		 		int s= ps.executeUpdate();
		 		if(s==1)
		 			return 1;
		 		else
		 			return 0;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			if (ps != null) {
		        try {
		            ps.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return 0;
	}
	public ArrayList<Traveler> getRecords(String BookingId)
	{
		ArrayList<Traveler> travelers = new ArrayList<Traveler>();
		ConnectDB c = new ConnectDB();
		Connection con = c.connect();
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			rs= st.executeQuery("select * from traveler where booking_id='"+BookingId+"'");
			while(rs.next())
			{
				Traveler t = new Traveler();
				t.bookingID = rs.getString(1);
				t.name= rs.getString(2);
				t.age = rs.getInt(3);
				t.sex = rs.getString(4);
				travelers.add(t);
			//	System.out.println(book.bookingID);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (st != null) {
		        try {
		            st.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (con != null) {
		        try {
		            con.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return travelers;
		
		
	}

}
