
drop table availability;

drop table traveler;
drop table tourDetails;
drop table booking;
drop table tour;
drop table hotels;
drop  table users;

create table users(login_id varchar2(20),first_name varchar2(20),last_name varchar2(20),
password varchar2(15),address varchar2(50),sex varchar2(10),
dob varchar2(15),contact varchar2(15),mail_id varchar2(40));

alter table users add primary key(login_id);
alter table users add unique(contact);
alter table users add unique(mail_id);

create table availability(booking_date varchar2(12),hotel_id varchar2(10),
rooms_remaining number(4));

alter table availability add primary key(booking_date,hotel_id);


create table hotels(hotel_id varchar2(20), name varchar2(30),place varchar2(30),
 price number(5));

alter table hotels add primary key(hotel_id);

create table traveler(booking_id varchar2(15), name varchar2(30),
age number(3),sex varchar2(20));

create table tour(package_id varchar2(20),package_name varchar2(200),starting_location varchar2(20),
price number(6),duration_days number(3));

alter table tour add primary key(package_id);

create table tourDetails(package_id varchar2(20),day number(3), hotel_id varchar2(20),
morning_slot varchar2(30),evening_slot varchar2(30),afternoon_slot varchar2(30));

alter table tourDetails add primary key(package_id,day,hotel_id);

create table booking(booking_id varchar2(20),package_id varchar2(20),login_id varchar2(20),
date_of_journey varchar2(12),total_travellers number(3));

alter table booking add primary key(booking_id);

alter table availability add foreign key(hotel_id) references hotels(hotel_id);
alter table traveler add foreign key(booking_id) references booking(booking_id);

alter table tourDetails add foreign key(package_id) references tour(package_id);
alter table tourDetails add foreign key(hotel_id) references hotels(hotel_id);

alter table booking add foreign key(login_id) references users(login_id);

alter table tourDetails drop (afternoon_slot,evening_slot,morning_slot);

alter table tourDetails add detail varchar2(2000);

alter table tourDetails drop primary key;

alter table tourDetails add primary key(package_id,day);

delete from tourDetails;

delete from tour;

insert into tour values('NB100','Central Dooars','NJP',9000,3);

insert into hotels values ('H103','Banani','Murti',1400);

insert into hotels values ('H104','Suntalekhola Nature Resort','Suntalekhola',1600);

insert into tourDetails values('NB100',1,'H103','Gear up for thrilling Zeep Safari at Chamramari Wildlife Sanctuary,which will take place in evening. Early morning Zeep Safari to Jatraprasad Rhino watch tower for which you have to wakeup very early the next day.');

insert into tourDetails values('NB100',2,'H103','Visit to Jhalong, Bindu in the lap of Himalaya. The area is known for its diversified flora and fauna.Jhalong is on the bank of the river Jaldhakaon the Indo-Bhutan border in the foothills of the Himalayas, on the way to Bindu. Major attractions are scenic beauty, birds and the Water body.Bindu is the last village of West Bengal before one enters Bhutan on the bank of river Jaladhaka a natural physical boundary separating West Bengal and Bhutan. Return to hotel at night.');

insert into tourDetails values('NB100',3,'H104','Visit Samsing, Suntaleykhola in the lap of Himalaya. The area is known for its diversified flora and fauna. Samsing is a village at around 3000 feet known for its landscape, tea gardens and the forest. It is also a gateway to Neora Valley National Park. Rolling Tea Estates are a treat to watch and SuntaleyKhola is named from a stream named Suntaley. One can spot exotic species of birds and butterflies. Have an exotic campfire at night at the resort.');

insert into tour values('NB101','West Dooars','NJP',10000,2);

insert into hotels values ('H101','Forest Bunglow Lava','Lava',1300);

insert into hotels values ('H102','Rishyop Hotel','Rishyop',1000);

insert into tourDetails values('NB101',1,'H101','Lava is a small hill station in Eastern Himalaya. After checking in and having done lunch, go for Lava local sightseeing covering Ratnarishi Bihar Buddhist Gumpha, Neora Interpretation Centre, Chhengey waterfalls. Overnight at Lava.');

insert into tourDetails values('NB101',2,'H102','Lolegaon is a beautiful village of Eastern Himalaya and famous for his natural beauty. After checking in and completing lunch, Lolegaon sightseeing covering Heritage Forest and nearby areas. Trasnfer to Rishyop at evening. Overnight at Rishyop.Watch the beautiful Kanchanzungha sunrise from your hotel room next morning.');


insert into tour values('NB102','East Dooars','NJP',7000,3);

insert into hotels values('H105','Jaldapara Tourist Lodge','Jaldapara',1500);


insert into hotels values('H106','Buxa Jungle Lodge','Buxa',1200);


insert into tourDetails values('NB102',1,'H105','After checking in at hotel visit Totopara villege. Free evening. Book your Elephant for next day elephant safari at Hollong. Safari next day morning.');

insert into tourDetails values('NB102',2,'H105','Visit Phuentsholling, it is a frontier town in southern Bhutan. Phuntsholing is the administrative seat of the Chukha district; a thriving commercial centre, it is home to a potpourri of ethnic groups intermingling at the point where the countries Bhutan and India meet.Temple called ZangthoPelriLhakhang, Monastery KharbandiGoemba, Crocodile breeding centerand the local market is the places of interest. Back to hotel at night.');

insert into tourDetails values('NB102',3,'H106','Buxaduar is the main center for ecotourism with over 300 species of trees, 250 species of shrubs, 400 species of herbs, over 350 species of birds have been identified so far - Jeep Safari through diversified flora and fauna of Buxa Tiger reserve and Pukhri Pahar, stay the night at hotel and enjoy the beauty of jungle');


insert into tour values('NB103','Darjeeling','NJP',11000,4);

insert into hotels values('H107','Darjeeling Tourist Lodge','Darjeeling',1000);

insert into tourDetails values('NB103',1,'H107','Arrive in Darjeeling, after checking in to your hotel and freshening up, come up to Chowrasta Mall, sit on a peripheral bench for a while, relax and enjoy the ambience and watch the activities. Then take a stroll along the scenic Mall Road and complete a circle.');


insert into tourDetails values('NB103',2,'H107','Go for the early morning 3-point tour which covers famous sunrise from Tiger Hill, Old Ghoom Monastery and Batasia Loop where the toy train takes a dramatic loop and negotiates an altitude of over 1,000ft. Later take the 7-point local tour which includes Himalayan Mountaineering Institute, Zoological Park, Ropeway, Tibetan Self Help Refugee Center, Lebong Race Course, Happy Valley Tea Estate etc. This tour will take about 4 hours.');

insert into tourDetails values('NB103',3,'H107',' Day trip to Mirik, enjoy the lake, do boating and some local sightseeing before returning to Darjeeling. On the way you should visit Jorpokhri (another nice place with lovely view and a water body), Simana which is a free way Indo-Nepal border with lovely views of Manebhanjan and mountains, and also Pashupati Market which is on Nepal side and known for its electronic goods.');

insert into tourDetails values('NB103',4,'H107','Take the 2-hour Toy Train Joyride from Darjeeling station in the morning. You will enjoy the scenic vistas from the heritage train (which is now part of Unesco World Heritage sites) as you ride the steam engine driven mini train with only two coaches. In the second half, visit the Rock Garden and Ganga Maya Park.');

insert into tour values('SB100','Kolkata','Esplanade',1000,1);

insert into hotels values('B100','AC BUS','Kolkata',1000);

insert into tourDetails values('SB100',1,'B100','Visit Victoria Memorial,Belur Math,Dakhineshwar Mandir by AC BUS');

insert into tour values('SB101','Sundarban','Esplanade',4000,1);

insert into hotels values('H108','MV Chitralekha','Vessel',4000);

insert into tourDetails values('SB101',1,'H108','Departure for Sonakhali from Esplanade by bus. Board onto MV Chitralekha. Visit Sajneykhali and Sudhanyakali watch tower in evening,stay the night at vessel. Visit Dobanki watchtower the next morning. Board off the vessel at Sonakhali then return to KOlkata by bus.');

insert into hotels values('Train','Train','Train',1000);
alter table booking add foreign key(package_id) references tour(package_id);