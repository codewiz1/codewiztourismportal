<!-- author Rajarsi Chattopadhyay -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="EntityClasses.*,java.util.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <title>--View Details-- </title>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <style>
       body  {
            background-image: url("Images/viewdet.jpg");
            background-repeat:no-repeat;
            background-size: 140%;
			background-position: center; 
   
             }
    </style>
  </head>
 <body>
  <%
     String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
  
  %>
 <nav class="navbar navbar-inverse">
             <div class="container-fluid">
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li><a href="home.jsp">Home</a></li>
                   <li><a href="AboutUs.html">About Us</a></li>
                    <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
                    <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
             </div>
         </nav> 
	 
	 
    <div class="container">
    <div class="row"> 
    <div class="jumbotron">
<%
Tour t = (Tour)session.getAttribute("tour");
Booking b = (Booking)session.getAttribute("bookingdet");
out.println("<h2>PackageName: "+t.getname()+"</h2>");
int rooms = b.gettravellers()/2;
if(rooms==0)
	rooms=1;
out.println("<h4>Package Price: Rs."+t.getprice()*rooms);
Calendar cal = b.getdateofjourney();
out.println("<h4>Starting Date: "+cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR));
out.println("<br>Package Duration: "+ t.getduration()+ " days");

out.println("<br> No. of Rooms booked: "+ rooms);
out.println("<br><br>TourDetails:");
out.println("<table class=\"table table-bordered table-striped\" ><tr><th>Day&nbsp;&nbsp;</th><th>Night Stay at </th><th>Details&nbsp;&nbsp;</th></tr>");
ArrayList<Tour_Details> td = t.getdetails();
for(Tour_Details t2 : td) {
	out.println("<tr>");
	out.println("<td>"+t2.getday()+"</td>");
	out.println("<td>"+t2.gethotelname()+"</td>");
    out.println("<td>"+t2.getdetail()+"</td>");
    
    //out.println("<td>"+t2.afternoonslot()+"</td>");
    //out.println("<td>"+t2.eveningslot()+"</td>");
    out.println("</tr>");    
    
}
out.println("</table>");


%>
</div>
</div>
</div>
<script src="Scripts/jquery.js"></script>
	<script src="Scripts/bootstrap.min.js"></script>
</body>
</html>