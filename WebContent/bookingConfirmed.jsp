<!-- author Rajarsi Chattopadhyay -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="EntityClasses.*,java.util.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html>
  <head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <title>--Confirmed-- </title>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
	 
    <style>
       body  {
            background-image: url("Images/confirmbg.jpg");
            background-repeat:no-repeat;
            background-size: 140%;
			background-position: center; 
   
             }
    </style>
  </head>
<body>
 <%
     String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
   if(session.getAttribute("booking") == null)
	 {
		 response.sendRedirect("login.html");
	 }
   else{
   Booking book = (Booking)session.getAttribute("booking");
   String date = (String)session.getAttribute("bdate");
  String id = book.getbookingID();
  String pkg = book.getpackageName();
  Calendar cal = book.getdateofjourney();
 // String date=  cal.get(Calendar.DATE) +"-"+ (cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.YEAR);
  int rooms = book.gettravellers()/2;
  if(rooms==0)
	  rooms=1;
  %>
<nav class="navbar navbar-inverse">
             <div class="container-fluid">
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li><a href="home.jsp">Home</a></li>
                   <li><a href="AboutUs.html">About Us</a></li>
                   <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
                   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                 </ul> 
             </div>
         </nav> 
	 
	 <br><br>
    <div class="container">
    <div class="jumbotron">
    <div class="row">
        <br>
       
        <div class="col-md-11" style="font-family:cursive;">
        <h2><b>Booking Details:</b><br></h2>
        </div>
       </div>
       
       <div class="row">
       <br>
       <div class="col-md-1"></div>
       
        <h3><b><div class="col-md-2">Booking Id</div> 
        <div class="col-md-3">Package Name</div>
        <div class="col-md-3">Date of Journey</div>
        <div class="col-md-3">Rooms Booked</div>
        </b>
        </div>
        <div class="row">
       <div class="col-md-1"></div>
       
        <h3><div class="col-md-2"><%= id %></div> 
        <div class="col-md-3"><%= pkg %></div>
        <div class="col-md-3"><%=date %></div>
        <div class="col-md-3"><%= rooms %></div>
        
        </div>
        <div class="row">
        <br>
        <div class="col-md-4"></div>
         <div class="col-md-4">
         <h2><b>Booking Confirmed!</b></h2>
         <br>
         <div align="center"><img src="Images/confirmed.jpg"  height="100" width="100"></div>
         </div>
         </div>
    </div>
 </div>
    <%} %>    
</body>
</html>