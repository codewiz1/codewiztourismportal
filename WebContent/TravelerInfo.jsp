<!-- author Rajarsi Chattopadhyay 

Bug:No validation for traveler name
Added Javascript for validation
Status fixed-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="EntityClasses.*,java.util.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <title>--Fill Traveler Details-- </title>
	 <script>
	 function check(count)
	 {
	 var i;
	 for(i=0;i<count;i++)
	 {
	 
	 var name= "id"+(i+1);
	 //alert(name);
	 
	 var word = document.getElementById(name).value;
	 //alert(word);
	   if (!(/^[a-zA-Z ]+$/.test(word))) {
	     bootbox.alert("Name should contain only aplahabets");
	     return false;
   // code
}
   age = "age"+(i+1);
    var age = document.getElementById(age).value;
    //alert(age);
    if (!(/^[0-9]+$/.test(age))) {
	     bootbox.alert("Enter proper value for age");
	     return false;
   // code
}

}
return true;
	 }
	 </script>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <style>
       body  {
            background-image: url("Images/travelerinfobg.jpg");
            background-repeat:no-repeat;
            background-size: cover;
			background-position: center; 
   
             }
    </style>
  </head>
	<body>
<%
     String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
  
  %>
 <nav class="navbar navbar-inverse">
             <div class="container-fluid">
             
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li ><a href="home.jsp">Home</a></li>
                   <li><a href="AboutUs.html">About Us</a></li>
                    <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
            
              
                  
				   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
            </div>
         </nav> 
	<div class="container">
	<div class="row">
	<div class="col-md-3"></div>
	  <div class="col-md-6">
	 <div class="jumbotron">
	<h2 style="font-family:cursive; color:CornflowerBlue ;"> Fill Traveler Details: </h2>  
	<h4>
	<br>
	<ol>
<form name="f1" method="post" action="BookingManagerServlet" onSubmit="return check(<%=(Integer)session.getAttribute("nooftravelers") %>)">
<%
	int nooftravelers=(Integer)session.getAttribute("nooftravelers");
	int count=0;
	while(count<nooftravelers)
	{	%>
		<li>  Name:<input type="text" id="id<%=count+1%>" name="name<%=count+1%>" required><br><br>&nbsp;&nbsp;Age:	<input type="text" size="4" id="age<%=count+1%>" name="age<%=count+1%>" required>
		&nbsp;&nbsp;Sex:<select name="sex<%=count+1%>"> 
  			<option value="Male">Male</option>
  			<option value="Female">Female</option>
  			
  		</select>
		<br><br>
		</li>
<% 	
	count++;
	}
%>


<input type="hidden" name="func" value="save">
&nbsp;&nbsp;&nbsp;<input type="submit" class="btn btn-success btn-lg" name="Save" value="Book">
</form>
</ol>
</div>
</div>
</div>
</div>
 <script src="Scripts/jquery-2.1.4.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/bootbox.min.js"></script>
</body>
</html>