<!-- Author Saikat Ghosh -->

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>--Plan Your Trip--</title>
<script>
	  function isDateSelected(){ 
	 // alert(document.f1.dateofjourney.value);
	 if (!document.f2.dateofjourney.value) {
	 return false;}
	  if(document.f2.dateofjourney.value == "mm/dd/yyyy" )
	    return false;
      var today =new Date();
      var inputDate = new Date(document.f2.dateofjourney.value);
     // alert(inputDate);
      if (inputDate.value == " "){
          return false;
      } else if (inputDate < (today.getTime()+86400000)) {
          return false;
      } else {
          return true;
      }
  }
     function chkbox()
	{
		var chkd = document.f2.c1.checked || document.f2.c2.checked|| document.f2.c3.checked|| document.f2.c4.checked ||document.f2.c5.checked|| document.f2.c6.checked;

		if (chkd == true)
		{
		return true;
		}
		else
		{
		bootbox.alert("Please select atleast one region.");
		return false;
		}

	}
     
     function check()
     {
     if(chkbox() === false)
        return false;
     
     if (isDateSelected() === false) {
   
     //document.getElementById("alert").style.display = 'block';
      bootbox.alert("Please enter a valid date of journey!");
      //f1.dateofjourney.focus();
      return false;
      } else {
      return true;
  }
     }
	 </script>
<!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
	<link href="Content/bootstrap-select.min.css" rel="stylesheet">
	
	<style>
	 .imageAndText {position: relative;
	               
	                width: 100px;
	                height: 50px;} 
     .imageAndText .col {position: absolute; z-index: 1; top: 0; left: 0; white-space: nowrap;}
	 
       body  {
            background-image: url("Images/tourPlanner.jpg");
            background-repeat:no-repeat;
            background-size: cover;
			background-position: center; 
   
             }
   

	</style>

</head>
<body>
  <%
     String username;
    username= (String)session.getAttribute("unm");
    //username= "SAIKAT";  //default input
  if(username==null)
	   response.sendRedirect("login.html");
  %>
  
     <nav class="navbar navbar-inverse">
             <div class="container-fluid">
             
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li ><a href="home.jsp">Home</a></li>
                   <li><a href="AboutUs.html">About Us</a></li>
                    <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
            
              
                  
				   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
            </div>
         </nav> 
	
         
		  <div class="container">
		    <div class="row">
			<div class="jumbotron">
		    
			  <h2 style="font-family:cursive;">We help you plan your trip...</h2>

			</div>
			</div>
		  </div>
		

		<div class="container">
		   
			
			<div class="jumbotron">
			 <div class="row">
			 <form name="f1" method="post" action="BookingManagerServlet">
             <input type="hidden" name="func" value="defaultSearch">
			 
			 <h2 style="font-family:cursive ;">Trust us and choose a package we have made for your luxurious travel..</h2>
			 <br>
			 <br>
			 
			 <div class="container">
			 <div class="row">
			                
							<div class="col-md-2">
							<div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" name="packageID" value="NB100" alt="Central Dooars" style="width:100%; height:100%">
							 <div class="col">
                              <h6 style="font-family:cursive; color:silver  ;">&nbsp;&nbsp; Central Dooars  </h6>
                             </div>
                             </div>
							 </div>
							 
							 <div class="col-md-2">
                             <div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" name="packageID" value="NB101" alt="West Dooars" style="width:100%; height:100%">
							 <div class="col">
                              <h6 style="font-family:cursive; color:silver  ;">&nbsp;&nbsp;West Dooars  </h6>
                             </div>
                             </div>
							 </div>
                             
							 <div class="col-md-2">
                             <div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" name="packageID" value="NB102" alt="East Dooars" style="width:100%; height:100%">
							 <div class="col">
                              <h6 style="font-family:cursive; color:silver  ;">&nbsp;&nbsp; East Dooars </h6>  
							  </div>
                             </div>
							 </div>
							 
							 <div class="col-md-2">
							 <div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" name="packageID" value="NB103" alt="Darjeeling" style="width:100%; height:100%">
							 <div class="col">
                              <h6 style="font-family:cursive; color:silver  ;">&nbsp;&nbsp; Darjeeling </h6>  
							  </div>
                             </div>
							 </div>
							 
							 <div class="col-md-2">
							 <div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" name="packageID" value="SB100" alt="Kolkata" style="width:100%; height:100%">
							 <div class="col">
                              <h6 style="font-family:cursive; color:silver  ;">&nbsp;&nbsp; Kolkata </h6>  
							  </div>
                             </div>
							 </div>
							 
							 <div class="col-md-2">
							 <div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" name="packageID" value="SB101" alt="Sundarban" style="width:100%; height:100%">
							 <div class="col">
                              <h6 style="font-family:cursive; color:silver  ;">&nbsp;&nbsp; Sundarbans </h6>  
							  </div>
                             </div>
							 </div>
			 
			 </div>
			 </div>
</form>
			</div>
			<br><br>
		   <div align="center"><h2>--OR--</h2></div>
		    <div class="row">
			<br><br>
			<h2 style="font-family:cursive ;">Let us help you build a package suitable for you..</h2>
			<br>
			<br>
			<center>
			 <form name="f2" method="post" action="BookingManagerServlet" onSubmit="return check()">
             <input type="hidden" name="func" value="customSearch">
			 
			 <h4 style="font-family:cursive; color:CornflowerBlue ;"> Select regions to visit :</h4>
			 
			 <label class="checkbox-inline">
             <input type="checkbox" name="c1" value="NB103">Darjeeling
             </label>
             <label class="checkbox-inline">
             <input type="checkbox" name="c2" value="NB102">Jaldapra - East Dooars
             </label>
             <label class="checkbox-inline">
             <input type="checkbox" name="c3" value="NB101">Lava Lolegaon - West Dooars
             </label>
			 <label class="checkbox-inline">
             <input type="checkbox" name="c4" value="NB100">Gorumara and Chapramari - Central Dooars
             </label>
             <label class="checkbox-inline">
             <input type="checkbox" name="c5" value="SB100">Kolkata
             </label>
             <label class="checkbox-inline">
             <input type="checkbox" name="c6" value="SB101">Sundarbans
             </label>
			 
			 
			 <br>
			 <br>
			 <h4 style="font-family:cursive; color:CornflowerBlue ;">Enter Date of journey:</h4> <input type="date" name="dateofjourney">
			 <br>
			 <br>
			 <h4 style="font-family:cursive; color:CornflowerBlue ;">Enter number of travelers:</h4>
			 
             <select name="nooftravelers"">
             <option value="1">1</option>
             <option value="2">2</option>
             <option value="3">3</option>
             <option value="4">4</option>
             <option value="5">5</option>
             <option value="6">6</option>
             <option value="7">7</option>
             <option value="8">8</option>
             </select>
             <br>
			 <br>
			 <h4 style="font-family:cursive; color:CornflowerBlue ;">Enter duration:</h4><select name="duration"> 
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			</select>
             <br>
			 <br>
			  <h4 style="font-family:cursive; color:CornflowerBlue ;">Enter Starting Location:</h4>
			  
			  <select name="startinglocation"> 
				<option value="NJP">New Jalpaiguri</option>
				<option value="Esplanade">Kolkata</option>
			</select>
			<br>
			<br>
			  <input type="Submit" class="btn btn-success btn-lg" value="Plan it for me">
			  
			 </form>
			 </center>
			 </div>
			 </div>
			 </div>



<!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="Scripts/jquery-2.1.4.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/bootbox.min.js"></script>
</body>
</html>
