<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="EntityClasses.*,java.util.*;"%>
<!-- Author Sagnik Ghosh -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <script>
	 function check(count)
	 {
	 var i;
	 for(i=0;i<count;i++)
	 {
	 
	 var name= "id"+(i+1);
	 //alert(name);
	 
	 var word = document.getElementById(name).value;
	 //alert(word);
	   if (!(/^[a-zA-Z ]+$/.test(word))) {
	     bootbox.alert("Name should contain only aplahabets");
	     return false;
   // code
}
   age = "age"+(i+1);
    var age = document.getElementById(age).value;
    //alert(age);
    if (!(/^[0-9]+$/.test(age))) {
	     bootbox.alert("Enter proper value for age");
	     return false;
   // code
}

}
return true;
	 }
	 </script>
	 <title>--Fill Traveler Details-- </title>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <style>
       body  {
            background-image: url("Images/travelerinfobg.jpg");
            background-repeat:no-repeat;
            background-size: cover;
			background-position: center; 
   
             }
    </style>
  </head>
<body>
<%
     String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
  
  %>
  <nav class="navbar navbar-inverse">
             <div class="container-fluid">
             
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li ><a href="home.jsp">Home</a></li>
                  <li><a href="AboutUs.html">About Us</a></li>
                  <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
            
              
                  
				   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
            </div>
         </nav> 
  
<div class="container">
    <div class="jumbotron">
    <div class="row">  
    <div class="col col-md-5">
<%
   if(session.getAttribute("tour") == null)
	   response.sendRedirect("login.html");
   else
	{Tour t=(Tour)session.getAttribute("tour");
	ArrayList<Tour_Details> details = t.getdetails();
	int size = details.size()-1;
	int i=0;
	out.println("<h3><b>Package Name: " + t.getname() +"<br><br>Starting Location: "+ t.getstartinglocation() +"<br><br>Duration: " +t.getduration()+" days.");
	out.println("<br><br>Details:</b><ul>");
	while(i<=size)
	{
		Tour_Details detail = details.get(i);
		int day=detail.getday();
		if (detail.gethotelID().equals("train"))
			out.println("<li><h4><b>" +"Day "+ day +":</b> "+  detail.getdetail() +"</li>");
		else 
			out.println("<li><h4><b>" +"Day "+ day +":</b> Check in at "+detail.gethotelname()+". " +detail.getdetail() +"</li>");
			//		out.print("<h6>" + detail.morningslot() + " next day morning."); 
		i++;
	}
	out.println("</ul>");
	}
%>
</div>
<div class="col col-md-1"></div>
<div class="col col-md-5">
<br><br>
<h3><b> Just fill in these details to book: <br>

	<form name="f1" method="post" action="BookingManagerServlet" onSubmit="return check(<%=(Integer)session.getAttribute("nooftravelers") %>)">
	<br>
<h4>Login ID: <input type="text" name="loginID" value="<%=username%>" readonly="true"> 
<br><br><br><b>Fill Traveler Details:</b><br><br>
<%
int nooftravelers=(Integer)session.getAttribute("nooftravelers");
int count=0;
while(count<nooftravelers)
{	%>
	<li>  Name: <input type="text" id="id<%=count+1%>" name="name<%=count+1%>" required><br><br>&nbsp;&nbsp; Age:	<input type="text" size="4" id="age<%=count+1%>" name="age<%=count+1%>" required>
	&nbsp;&nbsp;Sex: <select name="sex<%=count+1%>"> 
			<option value="Male">Male</option>
			<option value="Female">Female</option>
			
		</select>
	<br><br>
	</li>
<% 	
count++;
}
%><br>
<input type="hidden" name="func" value="bookCustom">
<div align="center"><input type="submit" class="btn btn-lg btn-success" name="Book" value="Book"></div>
</form>
 <script src="Scripts/jquery-2.1.4.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/bootbox.min.js"></script>
</body>
</html>