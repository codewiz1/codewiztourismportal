<!-- author Rajarsi Chattopadhyay -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="EntityClasses.*,java.util.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <title>--Tour Booking-- </title>
	 <script>
	  function isDateSelected(){ 
	 // alert(document.f1.dateofjourney.value);
	 if (!document.f1.dateofjourney.value) {
	 return false;}
	  if(document.f1.dateofjourney.value == "mm/dd/yyyy" )
	    return false;
      var today =new Date();
      var inputDate = new Date(document.f1.dateofjourney.value);
     // alert(inputDate);
      if (inputDate.value == " "){
          return false;
      } else if (inputDate < (today.getTime()+86400000)) {
          return false;
      } else {
          return true;
      }
  }
     function check()
     {
     if (isDateSelected() === false) {
   
     //document.getElementById("alert").style.display = 'block';
      bootbox.alert("Please enter a valid date of journey!");
      //f1.dateofjourney.focus();
      return false;
      } else {
      return true;
  }
     }
	 </script>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <style>
       body  {
            background-image: url("Images/tourInfobg.jpg");
            background-repeat:no-repeat;
            background-size: cover;
			background-position: center; 
   
             }
    </style>
  </head>
<body>
<%
     String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
  
  %>
 <nav class="navbar navbar-inverse">
             <div class="container-fluid">
       
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li ><a href="home.jsp">Home</a></li>
                  <li><a href="AboutUs.html">About Us</a></li>
                    <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
            
              
                  
				   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
            </div>
         </nav> 
	 
	<div class="container">
    <div class="jumbotron">
    <div class="row">  
    <div class="col col-md-5">
<%
   if(session.getAttribute("tour") == null)
	   response.sendRedirect("login.html");
   else
	{Tour t=(Tour)session.getAttribute("tour");
	ArrayList<Tour_Details> details = t.getdetails();
	int size = details.size()-1;
	int i=0;
	out.println("<h3><b>Package Name: " + t.getname() +"<br><br>Starting Location: "+ t.getstartinglocation() +"<br><br>Duration: " +t.getduration()+" days.");
	out.println("<br><br>Details:</b><ul>");
	while(i<=size)
	{
		Tour_Details detail = details.get(i);
		int day=detail.getday();
		out.println("<li><h4><b>" +"Day "+ day +":</b> Check in at "+detail.gethotelname()+". "+  detail.getdetail() +"</li>");
//		out.print("<h6>" + detail.morningslot() + " next day morning."); 
		i++;
	}
	out.println("</ul>");
	}
%>
</div>
<div class="col col-md-1"></div>
<div class="col col-md-5">
<br><br>
<h3><b> Just fill in these details to book: <br>
<form name="f1" method="post" action="BookingManagerServlet" onSubmit="return check()">
<br>
<h4>Login ID: <input type="text" name="loginID" value="<%=username%>" readonly="true"> <br>
<br>Date of journey: <input type="date" name="dateofjourney" > <br>
<br>No of travelers: <select name="nooftravelers"> 
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  <option value="8">8</option>
</select>
<br>
<input type="hidden" name="func" value="book">
<br><input type="submit"  class="btn btn-lg btn-success" name="Book" value="Book">
</form>
</div>
</div>
</div>
</div>
 <script src="Scripts/jquery-2.1.4.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/bootbox.min.js"></script>
</body>
</html>