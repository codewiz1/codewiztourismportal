<!-- author Rajarsi Chattopadhyay -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="EntityClasses.*,java.text.*,java.util.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <title>--Your Details-- </title>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <style>
       body  {
            background-image: url("Images/viewcancel.jpg");
            background-repeat:no-repeat;
            background-size: 140% 140%;
			background-position: center; 
   
             }
    </style>
  </head>
 
 <body>
 <%
 	String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
  
  %>
 <nav class="navbar navbar-inverse">
             <div class="container-fluid">
             
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li ><a href="home.jsp">Home</a></li>
                   <li><a href="AboutUs.html">About Us</a></li>
                   
                    <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
            
              
                  
				   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
            </div>
         </nav> 
	  <div class="container">
    <div class="row"> 
    <div class="jumbotron">
    <%
       user usr = new user();
       usr.getUserDetails(username);
       out.println("<h1>Hello "+usr.getName()+ "!!</h1>");
       out.println("<br><h2> Here are your details: </h2><h3>");
       out.println("<br> Email: "+usr.getmail() +" ");
       out.println("<br> Sex: "+usr.getsex() +" ");
       out.println("<br> Date of birth: "+usr.getdob() +" ");
       out.println("<br> Phone no: "+usr.getphno() +" ");
       out.println("<br> Address: "+usr.getaddress() +" ");
    
    %>
</body>
</html>