<!-- author Sagnik Ghosh -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <title>--Error-- </title>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
	<script>
        function redirect() {
            window.setTimeout("d()", "3000");
        }
        function d() {
            window.location = "TourPlanner.jsp";
        }
    </script>   
    <style>
       body  {
            background-image: url("Images/tourInfobg.jpg");
            background-repeat:no-repeat;
            background-size: 140%;
			background-position: center; 
   
             }
    </style>
  </head>
<body onload="redirect()">
<nav class="navbar navbar-inverse">
             <div class="container-fluid">
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li><a href="home.jsp">Home</a></li>
                   <li><a href="AboutUs.html">About Us</a></li>
                   <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
             </div>
         </nav> 
	 
	 
    <div class="container">
    <div class="jumbotron">
    <div class="row">
        
        <div class="col-md-6">
        <h2>Sorry tour cannot be generated!<br><br>Either hotels are not available or you have provided duration less than minimum requirement.<br><br>Please chose another duration or chose a different combination. </h2>
        </div>
        <div class="col-md-2"></div>
         <div class="col-md-4">
         <img src="Images/sorry.jpg">
         </div>
    </div>
 </div>
        
</body>
</html>