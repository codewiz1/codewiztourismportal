<!-- author Rajarsi Chattopadhyay -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="EntityClasses.*,java.text.*,java.util.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
     <meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
	 <title>--View/Cancel Booking-- </title>
     <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <style>
       body  {
            background-image: url("Images/viewcancel.jpg");
            background-repeat:no-repeat;
            background-size: 100% 100%;
			background-position: center; 
   
             }
    </style>
  </head>
 
 <body>
 <%
 	String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
  
  %>
 <nav class="navbar navbar-inverse">
             <div class="container-fluid">
             
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="#">Travel Bangla</a>
             </div>
                  <ul class="nav navbar-nav">
                   <li ><a href="home.jsp">Home</a></li>
                   <li><a href="AboutUs.html">About Us</a></li> 
                    <li><a href="ContactUs.html">Contact Us</a></li> 
                   </ul>
            
              
                  
				   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
            </div>
         </nav> 
	 
	 
    <div class="container">
    <div class="row"> 
    <div class="jumbotron">
<h1>View/Cancel Booking</h1>
<h3>Here are the tours that have been booked by you: <br></h3><h4>
<%try{
ArrayList<Booking> bookings = (ArrayList<Booking>)session.getAttribute("list");
out.println("<form name=\"f2\" method=\"post\" action=\"BookingManagerServlet\"> <br>");
out.println("<input type=\"hidden\" name=\"func\" value=\"viewcancel\">");
out.println("<table class=\"table table-bordered table-striped\" ><tr><th>Booking Id&nbsp;&nbsp;</th><th>Package Name&nbsp;&nbsp;</th><th>Date Of Journey&nbsp;&nbsp;</th><th></th><th></th></tr>");
for(Booking b : bookings) {
	out.println("<tr>");
	out.println("<td>"+b.getbookingID()+"</td>");
    out.println("<td>"+b.getpackageName()+"</td>");
    String temp = b.getdateofjourney().get(Calendar.DATE) +"-"+ (b.getdateofjourney().get(Calendar.MONTH)+1)+"-"+b.getdateofjourney().get(Calendar.YEAR);
    out.println("<td>"+temp+"</td>");
    Date today =new Date();
    SimpleDateFormat formatter = new SimpleDateFormat ("dd-MM-yyyy");
    Date d1=null;
    try{ d1 = formatter.parse(temp);
    } catch(Exception e)
    {
    	
    }
    long diff = d1.getTime() - today.getTime();
    if(diff>0)
    //out.println("<input type=\"hidden\" name=\"bookingDate\" value=\""+temp+"\">");
    {out.println("<td><button class=\"btn btn-success\" type=\"submit\" name=\"view\" value=\""+b.getbookingID()+"\">View Details</button></td>");
    out.println("<td>&nbsp;&nbsp;<button class=\"btn btn-warning\" type=\"submit\" name=\"cancel\" value=\""+b.getbookingID()+"\">Cancel Booking</button></td>");}
    else
    {
    	out.println("<td><button class=\"btn btn-success\" type=\"submit\" name=\"view\" value=\""+b.getbookingID()+"\">View Details</button></td>");
    	out.println("<td></td>");
    }
 	out.println("</tr>");    
}
out.println("</form>");
}
catch(Exception e)
{
   response.sendRedirect("login.html");
}%>
</h4>
</div>
</div>
</div>
<script src="Scripts/jquery.js"></script>
	<script src="Scripts/bootstrap.min.js"></script>
</body>
</html>