<!-- author Saikat Ghosh -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta charset="utf-8">
	 <meta name="veiwport" content="width=device-width, intial-scale=1.0">
<title>--Home--</title>
<!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
	<style>
	 .imageAndText {position: relative;
	                width: 300px;
	                height: 200px;} 
     .imageAndText .col {position: absolute; z-index: 1; top: 0; left: 0; white-space: nowrap;}
	
	body  {
            background-image: url("Images/homebg.jpg");
            background-repeat:no-repeat;
            background-size: cover;
			background-position: center; 
   
             }
	</style>
</head>
<body>
  <%
     String username;
    username=(String) session.getAttribute("unm");
   // username= "SAIKAT";  //default input
   if(username==null)
	   response.sendRedirect("login.html");
  
  %>
  <nav class="navbar navbar-inverse">
             <div class="container-fluid">
              <div class="navbar-header">
               <a class="navbar-brand" style="font-family:cursive; color:CornflowerBlue ;" href="home.jsp">Travel Bangla</a>
             </div>
                  
				   <ul class="nav navbar-nav navbar-right">
                 <li><a href="#"> Hello <% out.println(username); %>! </a></li>
				 <li>
                       <a href="Logout.jsp">
                       <span id="icon"> Logout</span>  
                       </a>
                  </li>
                   </ul> 
             </div>
         </nav> 
		 
		 <div class="container">
           <div class="row"> 
		  
             <div class="jumbotron">
                <h1 style="font-family:cursive;">Welcome to Travel Bangla</h1>

                    <p style="font-family:cursive;">Your luxurious travelling experience begins with us....</p>

                 
             </div>

		  </div>
		  </div>
		  
		  <div class="container">
           <div class="row"> 
             <div class="jumbotron">
                
				<div class="container">
                   <div class="row">
                     <div class="col-md-6">
                         <a href="UserInfo.jsp">
						     <div class="imageAndText">
                             <img src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" alt="Cinque Terre" style="width:100%; height:100%" >
                             <div class="col">
                             <div class="col-md-4">
                              <h2 style="font-family:cursive;  color:white   ;">Your Profile <br> Details</h2>
                             </div>
                             </div>
                             </div>
						 
						 </a>
                     </div>
                     <div class="col-md-6 ">
                         <a href="TourPlanner.jsp">
						     <div class="imageAndText">
                             <img src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" alt="Cinque Terre" style="width:100%; height:100%" >
                             <div class="col">
                             <div class="col-md-4">
                             <h2 style="font-family:cursive; color:white ;">Plan Your Trip</h2>
                             </div>
                             </div>
                             </div>
						 
						 </a>
                     </div>
                   </div>
				   <br>
				   <br>
				   <br>
				   <div class="row">
				   <form name="f1" method="post" action="BookingManagerServlet">
				   <input type="hidden" name="func" value="viewBooking">
                     <div class="col-md-6">
                        
						     <div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" alt="Cinque Terre" style="width:100%; height:100%" >
                             <div class="col">
                             <div class="col-md-4">
                              <h2 style="font-family:cursive; color:white  ;">Veiw Your Bookings</h2>
                             </div>
                             </div>
                             </div>
						 
						
                     </div>
                     
                     <div class="col-md-6 ">
                        
						     <div class="imageAndText">
                             <input type="image" src="Images/optionbg.jpg" class="align-center img-responsive img-rounded" alt="Cinque Terre" style="width:100%; height:100%" >
                             <div class="col">
                             <div class="col-md-4">
                             <h2 style="font-family:cursive; color:white ;">Cancel Bookings</h2>
                             </div>
                             </div>
                             </div>
						 
						
                     </div>
                   </form>
                   </div>
                   
				   <br>
				   <br>
				   <br>
				   <div class ="row">
				   <!--    <form action="InfoManager" autocomplete="off" class="form-horizontal" method="post" accept-charset="utf-8" >
                        <div class="input-group">
                        <h3><b> Select Region to Know more about it: </b>  <select name="region" width="300" style="width: 300px">
				             	<option value="Kolkata">Kolkata</option>
             					<option value="Sunderbans">Sunderbans</option>
             					<option value="Mirik">Mirik</option>
             					<option value="Rishop">Rishop</option>
             					<option value="Lava">Lava</option>
             					<option value="Samsing">Samsing</option>
             					<option value="Jhalong">Jhalong</option>
             					<option value="Phuntsholing">Phuntsholing</option>
             					<option value="Darjeeling">Darjeeling</option>
             					<option value="Jaldapara">Jaldapara</option>
             					<option value="Loleygaon">LoleyGaon</option>
             				</select>
             				<br>
                              <span class="input-group-btn">
                              <button class="btn btn-info btn-lg" type="submit" id="addressSearch">
                              Search
                              </button>
                              </span>
                         </div>
                        </form>
                        -->
                        <form action="InfoManager" autocomplete="off" class="form-horizontal" method="post" accept-charset="utf-8">
                        <div class="input-group">
                           <input name="region" value="" class="form-control" placeholder="Search Your Destination" type="text" required>
                              <span class="input-group-btn">
                             &nbsp;&nbsp; <button class="btn btn-info" type="submit" id="addressSearch">
                               Search
                              </button>
                              </span>
                         </div>
                        </form>
				   </div>
                </div>
                
				
				
				
				
             </div>
		  </div>
		  </div>
 <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Scripts/jquery.js"></script>
	<script src="Scripts/bootstrap.min.js"></script>
  </body>
</html>
